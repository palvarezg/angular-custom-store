import { BehaviorSubject, Observable, map, distinctUntilChanged } from 'rxjs';

export abstract class StoreBase<T> {
  protected store: BehaviorSubject<T>;

  constructor(private readonly initialState: T) {
    this.store = new BehaviorSubject(initialState);
  }

  public setSnapshot(snap: Partial<T>) {
    const currentState = this.getSnapshotValue();
    const nextState = Object.assign({}, currentState, snap);
    this.store.next(nextState);
  }

  public select(section: keyof T): Observable<T[keyof T]> {
    return this.store.pipe(
      map((snapshot: T) => {
        return snapshot[section];
      }),
      distinctUntilChanged()
    );
  }

  public getSnapshot(): Observable<T> {
    return this.store.pipe(distinctUntilChanged());
  }

  public getSnapshotValue(): T {
    return this.store.getValue();
  }

  public resetSnap() {
    this.store = new BehaviorSubject(this.initialState);
  }
}
