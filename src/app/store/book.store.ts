import { Injectable } from '@angular/core';
import { BookState } from './book.state';
import { StoreBase } from './store-base';
import { Observable, catchError, map, tap } from 'rxjs';
import { BookService } from '../services/books.service';
import { Book } from '../models/book.model';

@Injectable({
  providedIn: 'root',
})
export class BookStore extends StoreBase<BookState> {
  constructor(private bookService: BookService) {
    super({ books: [] });
  }

  addBook(book: Book): Observable<Book> {
    return this.bookService.addBook(book).pipe(
      map((book) => {
        const currentBooks = this.getSnapshotValue();
        const books = [...currentBooks.books, book];
        this.setSnapshot({ books });
        return book;
      })
    );
  }

  getBooks(): Observable<Book[]> {
    return this.bookService.getBooks().pipe(
      tap((books) => {
        this.setSnapshot({ books });
      }),
      catchError((err) => {
        console.error(err);
        throw err;
      })
    );
  }

  getBookById(id: number): Observable<Book> {
    return this.getSnapshot().pipe(
      map((state) => {
        const book = state.books.find((b) => b.id == id);
        if (!book) throw new Error(`Book with id ${id} not found`);
        return book;
      }),
      catchError((err) => {
        throw err;
      })
    );
  }

  deleteBook(id: number): Observable<Book> {
    const books = this.getSnapshotValue().books;
    const bookIndex = books.findIndex((b) => b.id === id);
    if (bookIndex === -1) throw new Error(`Book with id ${id} not found`);

    return this.bookService.deleteBook(books[bookIndex]).pipe(
      tap(() => {
        books.splice(bookIndex, 1);
        this.setSnapshot({ books });
      }),
      catchError((err) => {
        throw err;
      })
    );
  }
}
