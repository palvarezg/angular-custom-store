import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OnInit } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { Book } from '../models/book.model';
import { BookStore } from '../store/book.store';
import { catchError } from 'rxjs';

@Component({
  selector: 'app-books',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css'],
})
export class BooksComponent implements OnInit {
  books: Book[] = [];

  notificationMsg = '';
  error = false;

  searchedBook!: Book;

  idFormControl = new FormControl();
  titleFormControl = new FormControl();
  getByIdControl = new FormControl();
  deleteByIdControl = new FormControl();

  constructor(private store: BookStore) {}

  ngOnInit(): void {
    this.initDispatch();
    this.initiSubscriptions();
  }

  addBook() {
    this.store
      .addBook({
        id: this.idFormControl.value,
        title: this.titleFormControl.value,
      })
      .subscribe({
        next: (book) => this.showSuccessMsg(`Book ${book.title} added`),
        error: (err) => this.showErrorMsg(err),
      });

    this.idFormControl.reset();
    this.titleFormControl.reset();
  }

  searchById() {
    const id = this.getByIdControl.value;
    this.store.getBookById(id).subscribe((b) => (this.searchedBook = b));
  }

  deleteById() {
    const id = this.deleteByIdControl.value;
    this.store.deleteBook(id).subscribe({
      next: (book) => this.showSuccessMsg(`Book ${book.title} deleted`),
      error: (err) => this.showErrorMsg(err),
    });
  }

  private initDispatch() {
    this.store.getBooks().subscribe({
      next: (res) => this.showSuccessMsg(`${res.length} Books retrieved`),
      error: (err) => this.showErrorMsg(err),
    });
  }

  private initiSubscriptions() {
    this.store.getSnapshot().subscribe((state) => (this.books = state.books));
  }

  private showErrorMsg(msg: string) {
    this.error = true;
    this.notificationMsg = msg;
  }

  private showSuccessMsg(msg: string) {
    this.error = false;
    this.notificationMsg = msg;
  }
}
