import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Book } from '../models/book.model';

@Injectable({
  providedIn: 'root',
})
export class BookService {
  private booksList: Book[] = [
    { id: 0, title: 'Don Quijote de la Mancha' },
    { id: 1, title: 'Los Tres Cerditos' },
    { id: 2, title: 'Shreck 1' },
  ];

  constructor() {}

  addBook(book: Book): Observable<Book> {
    console.log(`Adding book ${{ book }}`);
    this.booksList = [...this.booksList, book];
    return of(book);
  }

  getBooks(): Observable<Book[]> {
    return of(this.booksList);
  }

  deleteBook(book: Book): Observable<Book> {
    console.log(`Delete book with id ${book.id}`);
    this.booksList = this.booksList.filter((b) => b.id !== book.id);
    return of(book);
  }
}
